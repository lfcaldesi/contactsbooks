import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { HeaderComponent } from "./header/header.component";

import { ContactsComponent } from "./contacts/contacts.component";
import { ContactListComponent } from "./contacts/contact-list/contact-list.component";
import { AddContactComponent } from "./contacts/add-contact/add-contact.component";
import { ContactDetailsComponent } from "./contacts/contact-details/contact-details.component";
import { ContactItemComponent } from "./contacts/contact-list/contact-item/contact-item.component";
import { AppRoutingModule } from "./app-routing.module";
import { ContactService } from "./contacts/service/contact.service";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { StoreModule } from "@ngrx/store";
import { contactListReducer } from "./contacts/store/contact-list.reducer";
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContactListComponent,
    AddContactComponent,
    ContactDetailsComponent,
    ContactItemComponent,
    ContactsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forRoot({ contactList: contactListReducer }),
  ],
  providers: [ContactService],
  bootstrap: [AppComponent],
})
export class AppModule {}
