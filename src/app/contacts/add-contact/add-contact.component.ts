import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
} from "@angular/forms";
import { Store } from "@ngrx/store";
import * as ContactListActions from "../store/contact-list.actions";
import { Contact } from "../model/contact.model";
@Component({
  selector: "app-add-contact",
  templateUrl: "./add-contact.component.html",
  styleUrls: ["./add-contact.component.css"],
})
export class AddContactComponent implements OnInit {
  id: number;

  contactForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,

    private router: Router,
    private store: Store<{ contactList: { contacts: Contact[] } }>
  ) {}

  ngOnInit() {
    this.initForm();
  }

  onSubmit() {
    this.store.dispatch(
      new ContactListActions.AddContact(this.contactForm.value)
    );
    this.onCancel();
  }

  onCancel() {
    this.router.navigate(["../"], { relativeTo: this.route });
  }

  private initForm() {
    let contactName = "";
    let contactLastName = "";
    let contactPhone = "";
    let contactMail = "";
    let contactAddress = "";

    this.contactForm = this.fb.group({
      name: new FormControl(contactName, Validators.required),
      lastName: new FormControl(contactLastName, Validators.required),
      phone: new FormControl(contactPhone, Validators.required),
      email: new FormControl(contactMail, Validators.required),
      address: new FormControl(contactAddress, Validators.required),
    });
  }
}
