import { Injectable } from "@angular/core";
import { Contact } from "../model/contact.model";
import { Subject } from "rxjs";

@Injectable()
export class ContactService {
  contactsChanged = new Subject<Contact[]>();
  private contacts: Contact[] = [
    new Contact("john", "smith", "111111111", "asdas@mail", "addr1"),
    new Contact("mary", "brown", "1231121223", "asdasd@ada", "addr2"),
    new Contact("mary", "black", "12312645653", "asdasd@ada", "addr3"),
    new Contact("mary", "white", "12314774456323", "aWWWd@ada", "addr4"),
    new Contact("john", "Doe", "123111146123", "QQQQdasd@ada", "addr5"),
  ];

  constructor() {}

  //Get all contacts

  getContacts() {
    return this.contacts.slice();
  }

  //Add contact

  addContact(contact: Contact) {
    this.contacts.push(contact);
    this.contactsChanged.next(this.contacts.slice());
  }

  //Get single contact by id

  getContact(index: number) {
    return this.contacts[index];
  }
}
