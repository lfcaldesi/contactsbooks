import { Action } from "@ngrx/store";
import { Contact } from "../model/contact.model";

export const ADD_CONTACT = "ADD_CONTACT";

export class AddContact implements Action {
  readonly type = ADD_CONTACT;

  constructor(public payload: Contact) {}
}

export type ContactListActions = AddContact;
