import * as ContactListAction from "./contact-list.actions";
import { Contact } from "../model/contact.model";

const initialState = {
  contacts: [
    new Contact("john", "smith", "111111111", "asdas@mail", "addr1"),
    new Contact("mary", "brown", "1231121223", "asdasd@ada", "addr2"),
    new Contact("mary", "black", "12312645653", "asdasd@ada", "addr3"),
    new Contact("mary", "white", "12314774456323", "aWWWd@ada", "addr4"),
    new Contact("john", "Doe", "123111146123", "QQQQdasd@ada", "addr5"),
  ],
};

export function contactListReducer(
  state = initialState,
  action: ContactListAction.ContactListActions
) {
  switch (action.type) {
    case ContactListAction.ADD_CONTACT:
      return {
        ...state,
        contacts: [...state.contacts, action.payload],
      };

    default:
      return state;
  }
}
