export class Contact {
  public name: string;
  public lastName: string;
  public phone: string;
  public email: string;
  public address: string;

  constructor(
    name: string,
    lastName: string,
    phone: string,
    email: string,
    address: string
  ) {
    this.name = name;
    this.lastName = lastName;
    this.phone = phone;
    this.email = email;
    this.address = address;
  }
}
