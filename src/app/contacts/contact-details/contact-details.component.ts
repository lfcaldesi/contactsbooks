import { Component, OnInit } from "@angular/core";
import { ContactService } from "../service/contact.service";
import { ActivatedRoute, Params } from "@angular/router";
import { Contact } from "../model/contact.model";
import { Store } from "@ngrx/store";
import { map } from "rxjs/operators";
@Component({
  selector: "app-contact-details",
  templateUrl: "./contact-details.component.html",
  styleUrls: ["./contact-details.component.css"],
})
export class ContactDetailsComponent implements OnInit {
  contact: Contact;
  id: number;

  constructor(
    private contactService: ContactService,
    private route: ActivatedRoute,
    private store: Store<{ contactList: { contacts: Contact[] } }>
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = +params["id"];

      this.store
        .select("contactList")
        .pipe(
          map((contactState) => {
            return contactState.contacts.find((contact, index) => {
              return index === this.id;
            });
          })
        )
        .subscribe((contact) => {
          this.contact = contact;
        });
    });
    //this.contact = this.contactService.getContact(this.id);
  }
}
