import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

import { Contact } from "../model/contact.model";
import { ContactService } from "../service/contact.service";
import { Subscription } from "rxjs";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
@Component({
  selector: "app-contact-list",
  templateUrl: "./contact-list.component.html",
  styleUrls: ["./contact-list.component.css"],
})
export class ContactListComponent implements OnInit {
  contacts: Observable<{ contacts: Contact[] }>;
  changeEvent: Subscription;
  constructor(
    private contactService: ContactService,
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<{ contactList: { contacts: Contact[] } }>
  ) {}

  ngOnInit() {
    this.contacts = this.store.select("contactList");
  }

  createNewContact() {
    this.router.navigate(["new"], { relativeTo: this.route });
  }
}
