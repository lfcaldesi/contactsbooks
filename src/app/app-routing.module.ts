import { Routes, RouterModule } from "@angular/router";
import { AddContactComponent } from "./contacts/add-contact/add-contact.component";
import { ContactDetailsComponent } from "./contacts/contact-details/contact-details.component";
import { ContactsComponent } from "./contacts/contacts.component";
import { NgModule } from "@angular/core";
const appRoutes: Routes = [
  { path: "", redirectTo: "/contacts", pathMatch: "full" },
  {
    path: "contacts",
    component: ContactsComponent,
    children: [
      { path: "new", component: AddContactComponent },
      { path: ":id", component: ContactDetailsComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
